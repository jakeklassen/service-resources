## cloud-init (user data) files

Some common scripts I've used when setting up new boxes.

After a box is up give it a few seconds and SSH in using the scripts indicated
user (if any), and use `tail -f /var/log/cloud-init-output.log` to track progress.

**Note:** Even though the box is connectable, there is still working being done,
so you can't just SSH in and hit use services that are being currently installed.
Make sure you track the progress in `/var/log/cloud-init-output.log`.
